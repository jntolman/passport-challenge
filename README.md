# README #

This app creates, reads, updates, and deletes renamable factories containing 1-15 nodes labeled with random numbers as determined by the set range.

### What technologies are used? ###

* JavaScript
    * Node.js
    * Express.js
    * Angular.js
    * Sockets.io
* Bootstrap
* PostgreSQL
* Git (Bitbucket)
* Heroku