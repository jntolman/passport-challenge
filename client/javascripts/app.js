
var angularApp = angular.module('nodeFactories', []);
angularApp.controller('mainController', ($scope, $http) => {
    $scope.formData = {};
    $scope.factoryData = {};
    // get all factories
    $http.get('/api/v1/factories')
    .success((data) => {
        for (let i in data) {
            if (data[i].children) {
                data[i].children = data[i].children.split(',');
            }
        }
        $scope.factoryData = data;
    })
    .error((error) => {
        console.log('Error: ' + error);
    });
    $scope.getFactories = () => {
        // get all factories
        $http.get('/api/v1/factories')
        .success((data) => {
            for (let i in data) {
                if (data[i].children) {
                    data[i].children = data[i].children.split(',');
                }
            }
            $scope.factoryData = data;
        })
        .error((error) => {
            console.log('Error: ' + error);
        });
    };
    $scope.addFactory = () => {
        $("#newFactoryModal").modal();
        $scope.formData = {};
    };
    // generate string of random numbers
    $scope.generateChildren = (children, lBound, uBound) => {
        let numArray = [];
        for (let i = 0; i < children; i++) {
            let ranNum = Math.floor((Math.random() * uBound) + lBound);
            if ( (ranNum >= lBound && ranNum <= uBound) && (numArray.indexOf(ranNum) < 0) ) {
                numArray.push(ranNum);
            } else {
                i--;
            }
        }
        return numArray.join(",");
    };
    // validate inputs
    $scope.validateInputs = (data, type) => {
        // if create form - type === 1, check for not null
        if (type === 1 && 
            (data.name === null || data.name === undefined ||
            data.lBound === null || data.lBound === undefined ||
            data.uBound === null || data.uBound === undefined ||
            data.children === null || data.children === undefined)){
                return false;
        }
        // check for all empty fields on update
        if (type === 2 && 
            (data.name === null || data.name === undefined) &&
            (data.lBound === null || data.lBound === undefined) &&
            (data.uBound === null || data.uBound === undefined) &&
            (data.children === null || data.children === undefined)){
                return false;
        }
        // check for special characters on not null name
        if (!(data.name === null || data.name === undefined) && data.name.match(/[^a-z0-9 ]+$/i)) {
            return false;
        }
        // check for special characters on not null children
        if (!(data.children === null || data.children === undefined) && data.children.match(/[^a-z0-9 ]+$/i)) {
            return false;
        }
         // verify bounds > 0 < 1000
        if (data.lBound < 1 || data.uBound > 999 || data.lBound >= data.uBound) {
            return false;
        }
        // check for appropriate number of children
        if (data.uBound - data.lBound < data.children || data.children > 15 || data.children < 1) {
            return false;
        }
        // passes tests
        return true;
    };
    // create a new factory
    $scope.createFactory = () => {
        // if inputs validate
        if ($scope.validateInputs($scope.formData, 1)) {
            $scope.formData.children = $scope.generateChildren($scope.formData.children, $scope.formData.lBound, $scope.formData.uBound);
            $("#newFactoryModal").modal("toggle"); 
            $http.post('/api/v1/factories', $scope.formData)
            .success((data) => {
                $scope.formData = {};
                for (let i in data) {
                    if (data[i].children) {
                        data[i].children = data[i].children.split(',');
                    }
                }
                $scope.factoryData = data;
            })
            .error((error) => {
                console.log('Error: ' + error);
            });
        }
    };
    $scope.updateFactory = (event) => {
        event.preventDefault();
        const req = {
            name: $scope.nameE,
            lBound: $scope.lboundE,
            uBound: $scope.uboundE,
            children: $scope.childrenE
        };
        // if inputs validate
        if ($scope.validateInputs($scope.editFormData, 2)) {
            if ($scope.editFormData.name) {
                req.name = $scope.editFormData.name.trim();
            }
            if ($scope.editFormData.lBound) {
                req.lBound = $scope.editFormData.lBound;
            }
            if ($scope.editFormData.uBound) {
                req.uBound = $scope.editFormData.uBound;
            }
            if ($scope.editFormData.children) {
                req.children = $scope.generateChildren($scope.editFormData.children.trim(), req.lBound, req.uBound);
            }
            $http.put('/api/v1/factories/' + $scope.editId, req)
            .success((data) => {
                for (let i in data) {
                    if (data[i].children) {
                        data[i].children = data[i].children.split(',');
                    }
                }
                $scope.factoryData = data;
            })
            .error((error) => {
                console.log('Error: ' + error);
            });
            $scope.editFormData = {};
            $("#editFactoryModal").modal("toggle");
        }
        
    };
    $scope.editFactory = (e) => {
        $scope.editId = e.target.id;
        $scope.nameE = e.target.dataset.n;
        $scope.lboundE = e.target.dataset.l;
        $scope.uboundE = e.target.dataset.u;
        $scope.childrenE = e.target.dataset.c.replace(/[^0-9\,]/g, "");
        $("#editFactoryModal").modal();
    };
    // delete a factory
    $scope.deleteFactory = () => {
        $("#editFactoryModal").modal("toggle");
        $http.delete('/api/v1/factories/' + $scope.editId)
        .success((data) => {
            for (let i in data) {
                if (data[i].children) {
                    data[i].children = data[i].children.split(',');
                }
            }
            $scope.factoryData = data;
        })
        .error((error) => {
            console.log('Error: ' + error);
        });
    };
});
