
const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/testdb';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query(
    'CREATE TABLE factories(id SERIAL PRIMARY KEY, name VARCHAR(40) not null, lBound INT, uBound INT, children VARCHAR)');
query.on('end', () => { client.end(); });
