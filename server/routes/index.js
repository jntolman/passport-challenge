
const express = require('express');
const router = express.Router();
const pg = require('pg');
const path = require('path');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/testdb';

// get home page
router.get('/', function(req, res, next) {
  res.sendFile(path.join(
      __dirname, '..', '..', 'client', 'views', 'index.html'));
});

module.exports = router;

// data validation
var validate = function (data) {
    // check for special characters
    if (data.name.match(/[^a-z0-9]+$/i)) {
            return false;
        }
    // check for duplicate children
    const testArray = data.children.split(',');
    let dupArray = [];
    for (let i = 0; i < testArray.length; i++) {
        if (dupArray.indexOf(testArray[i]) >= 0) {
            return false;
        }
        dupArray.push(testArray[i]);
    }
    // verify bounds > 0 < 1000
    if (data.lBound < 1 || data.uBound > 999 || data.lBound >= data.uBound) {
        return false;
    }
    // check for appropriate number of children
    if (data.uBound - data.lBound < testArray.length || testArray.length > 15) {
        return false;
    }
    // passes tests
    console.log("server pass");
    return true;
};

// create
router.post('/api/v1/factories', (req, res, next) => {
    const results = [];
    // get http request data
    const data = { name: req.body.name, lBound: req.body.lBound, uBound: req.body.uBound, children: req.body.children };
    // get postgres client from connection pool
    pg.connect(connectionString, (err, client, done) => {
        // handle errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        if (validate(data)) {
            // sql query >> insert data
            client.query('INSERT INTO factories(name, lBound, uBound, children) values($1, $2, $3, $4)',
            [data.name.trim(), data.lBound, data.uBound, data.children]);
            // sql query >> select data
            const query = client.query('SELECT * FROM factories ORDER BY id ASC');
            // stream results one row at a time
            query.on('row', (row) => {
                results.push(row);
            });
            // after all data is returned, close connection and return results
            query.on('end', () => {
                done();
                return res.json(results);
            });
        } else {
            done();
            console.log("Bad entry.");
            return res.status(500).json({success: false, data: "Bad entry."});
        }
    });
});

// read
router.get('/api/v1/factories', (req, res, next) => {
    const results = [];
    // get postgres client from connection pool
    pg.connect(connectionString, (err, client, done) => {
        // handle errors
        if (err) {
            done();
            console.log(err);
            res.status(500).json({success: false, data: err});
        }
        // sql query >> select data
        const query = client.query('SELECT * FROM factories ORDER BY id ASC');
        // stream results one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // after data is returned, close connect and return results
        query.on('end', () => {
            done();
            return res.json(results);
        });
    });
});

// update
router.put('/api/v1/factories/:factory_id', (req, res, next) => {
    const results = [];
    // grab data from url parameters
    const id = req.params.factory_id;
    // grab data from http request
    const data = { name: req.body.name, lBound: req.body.lBound, uBound: req.body.uBound, children: req.body.children };
    // get postgres client from connection pool
    pg.connect(connectionString, (err, client, done) => {
        // handle errors
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({success: false, data: err});
        }
        if (validate(data)) {
            // sql query >> update data
            client.query('UPDATE factories SET name=($1), lBound=($2), uBound=($3), children=($4) WHERE id=($5)',
            [data.name, data.lBound, data.uBound, data.children, id]);
            // sql query >> select data
            const query = client.query('SELECT * FROM factories ORDER BY id ASC');
            // stream results one row at a time
            query.on('row', (row) => {
                results.push(row);
            });
            // after all the data, close connect and return results
            query.on('end', () => {
                done();
                return res.json(results);
            });
        } else {
            done();
            console.log("Bad entry.");
            return res.status(500).json({success: false, data: "Bad entry."});
        }
    });
});

// delete
router.delete('/api/v1/factories/:factory_id', (req, res, next) => {
    const results = [];
    // grab data from url parameters
    const id = req.params.factory_id;
    // get postgress client from connection pool
    pg.connect(connectionString, (err, client, done) => {
        // handle errors
        if (err) {
            done();
            console.log(err);
            res.status(500).json({success: false, data: err});
        }
        // sql query >> delete data
        client.query('DELETE FROM factories WHERE id=($1)', [id]);
        // sql query >> select data
        var query = client.query('SELECT * FROM factories ORDER BY id ASC');
        // stream results one row at a time
        query.on('row', (row) => {
            results.push(row);
        });
        // after data is returned, close connection and return results
        query.on('end', () => {
            done();
            return res.json(results);
        });
    });
});
