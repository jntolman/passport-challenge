// socket function on server
module.exports = function(io) {
    io.on('connection', (socket) => {
        console.log('Client connected');
        socket.on('news', function (data) {
            io.sockets.emit('news', data);
        });
    socket.on('disconnect', () => console.log('Client disconnected'));
    });
};
